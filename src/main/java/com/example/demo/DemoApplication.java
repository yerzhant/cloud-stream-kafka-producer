package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.Date;

@SpringBootApplication
@EnableBinding(Source.class)
@EnableSchemaRegistryClient
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Component
    public class CmdRunner implements CommandLineRunner {

        @Autowired
        private Source source;

        @Override
        public void run(String... strings) {
            MyMessage myMessage = new MyMessage(12345, "Hi there111!!!", new Date());
            source.output().send(MessageBuilder.withPayload(myMessage).build());
            System.err.println("Sent");
        }
    }

}
