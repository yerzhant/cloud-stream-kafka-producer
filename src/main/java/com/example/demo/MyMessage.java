package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
class MyMessage {
    private Integer id;
    private String msg;
    private Date date;
}
